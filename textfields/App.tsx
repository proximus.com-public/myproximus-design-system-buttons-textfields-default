import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import {
  epicTheme,
  ThemeProvider,
} from '@proximus/react-native-common-ui/lib/styling';
import PxTextInput from '@proximus/react-native-common-ui/lib/inputs/PxTextInput';
import { useFonts } from 'expo-font';

export default function App() {
  const loaded = useFonts({
    'Proximus-Bold': require('@proximus/react-native-font-icons/lib/assets/fonts/Proximus-Bold.ttf'),
    'Proximus-Regular': require('@proximus/react-native-font-icons/lib/assets/fonts/Proximus-Regular.ttf'),
  });

  const [inputValue, setInputValue] = useState('');

  if (!loaded) {
    return <View />;
  }

  return (
    <ThemeProvider theme={epicTheme}>
      <View style={styles.container}>
        <PxTextInput
          style={{ height: 50 }}
          placeholder="This is a placeholder"
          value={inputValue}
          onChangeText={setInputValue}
          inputLabelText="Input label (optional)"
        />
      </View>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
});
