import React, { useState } from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {
  epicTheme,
  ThemeProvider,
} from '@proximus/react-native-common-ui/lib/styling';
import PxTextInput from '@proximus/react-native-common-ui/lib/inputs/PxTextInput';
import { useFonts } from 'expo-font';
import Constants from 'expo-constants';

import TextFields from './textfields/App'


export default function () {
  return <>
    {Constants.manifest.extra.snackToLoad === 'textfields' && <TextFields/>}
    </>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
});
